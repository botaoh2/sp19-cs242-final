import unittest
import database

mydb = database.mydb

class TestDatabase(unittest.TestCase):

	# test insert/delete
	def test_player(self):
		print('test player insert/delete')
		database.insert_player('#test_tag')
		self.assertTrue(database.contains_player('#test_tag'))
		self.assertEqual(database.delete_player('#test_tag'), 1)
		self.assertFalse(database.contains_player('#test_tag'))

	# test update
	def test_player_update(self):
		print('test player update')
		database.insert_player('#test_tag')
		self.assertTrue(database.contains_player('#test_tag'))
		database.update_player('#test_tag')

		sql = "SELECT id, tag, update_time FROM player WHERE tag = %s"
		val = ('#test_tag', )
		mycursor = mydb.cursor()
		mycursor.execute(sql, val)
		myresult = mycursor.fetchall()
		mycursor.close()
		result = myresult
		self.assertEqual(len(result), 1)
		self.assertEqual(result[0][1], '#test_tag')
		self.assertTrue(result[0][2] != None)
		database.delete_player('#test_tag')

	# test insert battle
	def test_battle_insert(self):
		print('test battle insert')
		self.assertTrue(database.insert_battle('#test_battle_tag', 'some random json data'))
		self.assertTrue(database.contains_battle('#test_battle_tag'))
		mycursor = mydb.cursor()
		mycursor.execute('DELETE FROM battlelog WHERE tag = %s', ('#test_battle_tag', ))
		mydb.commit()
		mycursor.close()

if __name__ == '__main__':
	unittest.main()