CREATE TABLE battlelog(
	id BIGINT UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    tag VARCHAR(64) NOT NULL,
    json_data BLOB
);

CREATE TABLE player (
	id BIGINT UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    tag VARCHAR(16) NOT NULL,
    update_time DATETIME
);

CREATE INDEX update_time ON player(update_time);
CREATE INDEX tag ON player(tag);

CREATE INDEX tag ON battlelog(tag);