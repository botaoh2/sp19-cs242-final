import requests
import logging
import time
import database
import json
import random
import config

card_set = set()
type_set = set()
arena_set = set()
deckSelection_set = set()
gameMode_set = set()

player_queue = []

# get next player tag to scrape
def next_player():
	global player_queue
	if len(player_queue) == 0:
		player_queue = database.next_players()
	return player_queue.pop()

# add player to database for future scrape
def add_player(tag):
	database.insert_player(tag)

# request battlelog of given player tag
def request_battlelog(tag):
	tag = tag.replace('#', '%23')
	url = "https://api.clashroyale.com/v1/players/{}/battlelog".format(tag)
	headers = {
		'authorization': config.auth_token
	}
	r = requests.get(url = url, headers = headers)
	return r.json()

# add opponent battle tag to database
def queue_opponent_tag(battle):
	opponent_tag = battle['opponent'][0]['tag']
	add_player(opponent_tag)
	# 2v2
	if len(battle['opponent']) > 1:
		add_player(battle['team'][1]['tag'])
		add_player(battle['opponent'][1]['tag'])

# create unique battle tag from player lowest player tag and battleTime
def create_battle_tag(battle):
	player_tags = []
	for player in battle['team']:
		player_tags.append(player['tag'])
	for player in battle['opponent']:
		player_tags.append(player['tag'])
	battle_tag = min(player_tags) + '#' + battle['battleTime']
	return battle_tag

# extract useful information from battle
def extract_data(battle):
	result = {}
	result['type'] = battle['type']
	result['battleTime'] = battle['battleTime']
	result['isLadderTournament'] = battle['isLadderTournament']
	result['arena'] = battle['arena']['name']
	result['gameMode'] = battle['gameMode']['name']
	result['deckSelection'] = battle['deckSelection']

	result['team'] = []
	for team in battle['team']:
		result_team = {}
		result_team['tag'] = team['tag']
		result_team['crowns'] = team['crowns']
		result_team['cards'] = []
		for card in team['cards']:
			result_card = {}
			result_card['level'] = card['level'] + 13 - card['maxLevel']
			result_card['name'] = card['name']
			result_team['cards'].append(result_card)
		result['team'].append(result_team)

	result['opponent'] = []
	for team in battle['opponent']:
		result_team = {}
		result_team['tag'] = team['tag']
		result_team['crowns'] = team['crowns']
		result_team['cards'] = []
		for card in team['cards']:
			result_card = {}
			result_card['level'] = card['level'] + 13 - card['maxLevel']
			result_card['name'] = card['name']
			result_team['cards'].append(result_card)
		result['opponent'].append(result_team)
	return result

# add battle to database
def add_battle(battle):
	tag = create_battle_tag(battle)
	battle_data = extract_data(battle)
	#analyze_data(battle_data)
	return database.insert_battle(tag, battle_data)

def analyze_data(battle):
	type_set.add(battle['type'])
	arena_set.add(battle['arena'])
	deckSelection_set.add(battle['deckSelection'])
	gameMode_set.add(battle['gameMode'])
	for card in battle['opponent'][0]['cards']:
		card_set.add(card['name'])
	for card in battle['team'][0]['cards']:
		card_set.add(card['name'])
	if random.random() < 0.01:
		logging.critical('type_set: %s', type_set)
		logging.critical('arena_set: %s', arena_set)
		logging.critical('deckSelection_set: %s', deckSelection_set)
		logging.critical('gameMode_set: %s', gameMode_set)
		logging.critical('card_set: %s', card_set)
		logging.critical('')

def run():
	while True:
		time.sleep(1)
		player_tag = next_player()
		battlelog = request_battlelog(player_tag)
		valid_battle_count = 0
		for battle in battlelog:
			queue_opponent_tag(battle)
			if add_battle(battle):
				valid_battle_count += 1
		database.update_player(player_tag)
		logging.info('%d new battles fetched from player %s', valid_battle_count, player_tag)


run()





