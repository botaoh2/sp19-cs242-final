import logging
import config
import database
import json
from datetime import datetime

type_set = {'clanWarCollectionDay', 'challenge', 'clanMate', 'clanWarWarDay', \
'tournament', '2v2', 'PvP', 'clanMate2v2', 'friendly'}

arena_set = {'Arena 13', 'Training Camp', 'Arena 5', 'Arena 6', 'Arena 11', \
'Arena 8', 'Arena 2', 'Arena 10', 'Arena 4', 'Arena 7', 'Clan League', 'Arena 12', \
'League 3', 'Arena 9', 'Arena 3', 'League 2', 'Arena 1'}

deckSelection_set = {'predefined', 'eventDeck', 'collection', 'draft'}

gameMode_set = {'TripleElixir_Tournament', 'TeamVsTeam_DoubleElixir_Ladder', \
'Rage_Ladder', 'DraftMode', 'TeamVsTeamDraft', 'TeamVsTeam_FixedDeckOrder', \
'Draft_Rage_SpawnJacks_Friendly', 'Ladder', 'Challenge', 'DoubleElixir', \
'ClassicDecks_Friendly', 'EventDeck_Rage_SpawnJacks_Friendly', 'Showdown_Ladder', \
'TeamVsTeamLadder', 'Ladder_GoldRush', 'TeamVsTeam', 'DoubleElixir_Ladder', 'DraftModeInsane', 'Friendly'}

card_set = {'Archers', 'Battle Ram', 'Royal Giant', 'Elite Barbarians', 'Lava Hound', \
'Bandit', 'Flying Machine', 'Fireball', 'P.E.K.K.A', 'Barbarian Barrel', 'Magic Archer', \
'The Log', 'Wall Breakers', 'Night Witch', 'Mortar', 'Minion Horde', 'Barbarians', \
'Fire Spirits', 'Freeze', 'Giant Skeleton', 'Lightning', 'Zappies', 'Mini P.E.K.K.A', \
'Minions', 'Goblin Barrel', 'Zap', 'Miner', 'Ram Rider', 'Barbarian Hut', 'Tornado', \
'Witch', 'Guards', 'Inferno Dragon', 'Arrows', 'Mega Knight', 'Goblin Giant', 'Goblins', \
'Ice Wizard', 'Rocket', 'Cannon Cart', 'Baby Dragon', 'Dark Prince', 'Giant Snowball', \
'X-Bow', 'Sparky', 'Golem', 'Goblin Gang', 'Giant', 'Inferno Tower', 'Prince', 'Bomb Tower', \
'Knight', 'Spear Goblins', 'Bomber', 'Tesla', 'Skeletons', 'Lumberjack', 'Clone', \
'Elixir Collector', 'Hunter', 'Ice Spirit', 'Electro Dragon', 'Ice Golem', 'Heal', \
'Royal Ghost', 'Dart Goblin', 'Cannon', 'Balloon', 'Rage', 'Executioner', 'Tombstone', \
'Mega Minion', 'Skeleton Barrel', 'Electro Wizard', 'Hog Rider', 'Musketeer', 'Bats', \
'Graveyard', 'Valkyrie', 'Skeleton Army', 'Three Musketeers', 'Royal Recruits', 'Princess', \
'Furnace', 'Goblin Hut', 'Mirror', 'Royal Hogs', 'Rascals', 'Poison', 'Wizard', 'Bowler'}

def datetime_filter(battle):
	battle_time = datetime.strptime(battle['battleTime'], "%Y%m%dT%H%M%S.000Z")
	start_datetime = datetime(year = 2019, month = 4, day = 2)
	return battle_time > start_datetime

def pvp_ladder_filter(battle):
	return battle['gameMode'] == 'Ladder' and battle['type'] == 'PvP'

filters = [pvp_ladder_filter, datetime_filter]

battle_list = database.get_battles()

for f in filters:
	battle_list = filter(f, battle_list)

battle_list = list(battle_list)
logging.info("size after filter: %d", len(battle_list))
logging.info('finish')



