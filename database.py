import mysql.connector
import config
import json
import logging

mydb = mysql.connector.connect(
	host = config.host, 
	user = config.user, 
	passwd = config.password,
	database = config.database
)

# return True if database contains player tag, False otherwise
def contains_player(tag):
	logger = logging.getLogger('contains_player')
	sql = "SELECT COUNT(tag) FROM player WHERE tag = %s;"
	val = (tag, )

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	myresult = mycursor.fetchall()
	mycursor.close()

	return myresult[0][0] > 0

# insert player with given tag into database
def insert_player(tag):
	logger = logging.getLogger('insert_player')
	if contains_player(tag):
		logger.debug("player %s is already in database", tag)
		return False

	sql = "INSERT INTO player (tag) VALUES (%s);"
	val = (tag, )

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	mydb.commit()
	mycursor.close()

	logger.debug("player %s inserted into database", tag)
	return True

# update player update_time with current time
def update_player(tag):
	logger = logging.getLogger('update_player')
	sql = "UPDATE player SET update_time = NOW() where tag = %s;"
	val = (tag, )

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	mydb.commit()
	mycursor.close()

	if mycursor.rowcount > 0 :
		logger.debug("player %s updated", tag)
	else:
		logger.warn("player with tag %s not found", tag)

# return a list of player tags to scrape next ordered by update_time
def next_players(limit = 10):
	logger = logging.getLogger('next_players')
	sql = "SELECT tag FROM player ORDER BY update_time LIMIT %s"
	val = (limit, )

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	myresult = mycursor.fetchall()
	mycursor.close()
	result = map(lambda x : x[0], myresult)
	result = list(result)

	logger.info("%d players fetched from database", len(result))
	return result

# delete player (only for testing)
def delete_player(tag):
	logger = logging.getLogger('delete_player')
	sql = "DELETE FROM player WHERE tag = %s"
	val = (tag, )

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	mydb.commit()
	result = mycursor.rowcount
	mycursor.close()

	logger.debug("%d player(s) deleted from database", result)
	return result

# return True if database contains battle tag, False otherwise
def contains_battle(tag):
	logger = logging.getLogger('contains_battle')
	sql = "SELECT COUNT(tag) FROM battlelog WHERE tag = %s;"
	val = (tag, )

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	myresult = mycursor.fetchall()
	mycursor.close()

	return myresult[0][0] > 0

# insert battle into database
def insert_battle(tag, battle):
	logger = logging.getLogger('insert_battle')
	if contains_battle(tag):
		logger.debug("battle %s is already in database", tag)
		return False

	sql = "INSERT INTO battlelog (tag, json_data) VALUES (%s, %s);"
	val = (tag, json.dumps(battle))

	mycursor = mydb.cursor()
	mycursor.execute(sql, val)
	mydb.commit()
	mycursor.close()

	logger.debug("battle %s inserted into database", tag)
	return True

# fetch all battles from database
def get_battles():
	logger = logging.getLogger('get_battles')
	logger.info("fetching all battles... this may take some time")
	sql = "SELECT json_data FROM battlelog"

	mycursor = mydb.cursor()
	mycursor.execute(sql)

	myresult = mycursor.fetchall()
	logger.info("fetched total %d result", len(myresult))
	result = map(lambda x : json.loads(x[0]), myresult)
	return result
